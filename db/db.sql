CREATE TABLE Movie (movie_id integer primary key, movie_title varchar(20), movie_release_date varchar(20), movie_time varchar(20), director_name varchar(20));


INSERT INTO Movie VALUES (1, 'Atrangi Re', '20 Dec 2021', '2hr 30min', 'Dhanush');
INSERT INTO Movie VALUES (2, 'Pushpa', '20 Nov 2021', '3hr 30min', 'Allu Arjun');
INSERT INTO Movie VALUES (3, 'Spider Man', '27 Dec 2021', '1hr 35min', 'Tom Holland');
INSERT INTO Movie VALUES (4, 'Avengers', '20 Apr 2012', '2hr 40min', 'Robert Downey');
INSERT INTO Movie VALUES (5, 'Antim', '19 Oct 2021', '2hr 06min', 'Salman Khan');
INSERT INTO Movie VALUES (6, 'Wrong Turn 7', '05 Jul 2021', '1hr 30min', 'Natalie Portman');
INSERT INTO Movie VALUES (7, 'Iron Man', '07 Mar 2008', '2hr 47min', 'Tony Stark');
INSERT INTO Movie VALUES (8, 'Raees', '23 Feb 2017', '1hr 55min', 'Shah Rukh Khan');





